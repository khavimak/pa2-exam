#include <iostream>
#include <deque>

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
	using std::deque<TreeNode *> = deck;
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
	deck pueue{p};
	deck queue{q};
	while(!pue.empty() && !que.empty()) {
		TreeNode * p = pue.front()
		pue.pop_front();
		pue.push_back(p.left);
		pue.push_back(p.right);

		TreeNode * q = que.front()
		que.pop_front();
		que.push_back(q.left);
		que.push_back(q.right);
		if(q.val != p.val)
			return false;
	}
	return pue.empty() && que.empty();
    }
};
