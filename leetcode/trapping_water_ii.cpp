#include <vector>
#include <map>
#include <array>
#include <stdexcept>
#include <cstdlib>
using namespace std;

class Solution {
    using MTR = vector<vector<int>>;
public:
    int trapRainWater(const MTR & heightMap) {
        set_map(heightMap);
        int res = 0;
        
        for(int i = 0; i < m; ++i){
            for(int j = 0; j < n; ++j){
                for(int k = min_h; k < max_h; ++k )
                    res += holds(CCoord{i, j, k});
            }
        }
        return res;
    }

private:
    struct CCoord{
        int x, y, z;
        CCoord(int X, int Y, int Z) : x(X), y(Y), z(Z) {}

        static std::array<CCoord, 5> adj(const CCoord & c){
            return {CCoord{c.x, c.y, c.z - 1},
                    CCoord{c.x, c.y - 1, c.z},
                    CCoord{c.x, c.y + 1, c.z},
                    CCoord{c.x - 1, c.y, c.z},
                    CCoord{c.x + 1, c.y, c.z}};
        }

        friend bool operator < (const CCoord & lhs, const CCoord & rhs){
            return  lhs.x != rhs.x ? lhs.x < rhs.x :
                    lhs.y != rhs.y ? lhs.y < rhs.y : 
                                     lhs.z < rhs.z;
        }
    };

    void set_map(const MTR & heightMap) {
        hm = &heightMap;
        mem.clear();
        m = hm->size();
        n = (*hm)[0].size();

        for(size_t i = 0; i < m; ++i){
            for(size_t j = 0; j < n; ++j){
                auto x = (*hm)[i][j];
                if(x > max_h)
                    max_h = x;
                if(x < min_h - 1)
                    min_h = x + 1;
            }
        }
    }

    bool holds(const CCoord & c) {
        if( is_valid(c))
            return false;
        if((*hm)[c.x][c.y] >= c.z)
            return true;
        try{
            return mem.at(c);
        }catch(const std::out_of_range & ){
            for(const CCoord & t : CCoord::adj(c)){
                if(!holds(t)){
                    mem[c] = false;
                    return false;
                }
            }
            mem[c] = true;
            return true;
        }
    }

    bool is_valid(const CCoord & c) const {
        return 0 < c.x && c.x < m &&
               0 < c.y && c.y < n &&
               0 < c.z && c.z < max_h;
    }

    const MTR * hm = nullptr;
    int m = 0, n = 0, max_h = 0, min_h = 1 << 20;
    std::map<CCoord, bool> mem{};
};