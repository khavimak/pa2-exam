#include <stdexcept>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
    bool canFinish(int numCourses, const vector<vector<int>>& prerequisites) {
        make_prereq(prerequisites);

        for(int i = 0; i < numCourses; ++i){
            if(depends_on_cycle(i))
                return false;
        }
        return true;
    }

private:
    enum class ENodeState : bool {OPEN, CLOSED};

    void make_prereq(const vector<vector<int>>& prerequisites){
        for(const auto & p : prerequisites){
            if(auto it = prereq.find(p[0]); it != prereq.end())
                it->second.push_back(p[1]);
            else
                prereq.emplace(p[0], std::vector<int>{p[1]});
        }
    }

    bool depends_on_cycle(int n){
        using enum ENodeState;
        if(auto it = state_map.find(n); it != state_map.end()){
            return it->second == OPEN;
        }
        state_map.emplace(n, OPEN);

        
        for(int x : prereq[n]){
            if(depends_on_cycle(x))
                return true;
        }
        state_map[n] = CLOSED;
        return false;
    }

    std::unordered_map<int, vector<int>> prereq{};
    std::unordered_map<int, ENodeState> state_map{};
};