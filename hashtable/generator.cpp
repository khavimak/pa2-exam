#include <iostream>
#include <cstdlib>
#include <string>


int main(int argc, char * argv[]){
  srand(time(0));
  size_t n = std::stoul(argv[1]);
  for(size_t i = 0; i < n; ++i){
    char c = rand() % 3;
    switch(c){
      case 0:
        std::cout << "i " << rand() << '\n';
        break;
      case 1:
        std::cout << "r " << rand() << '\n';
        break;
      case 2:
        std::cout << "c " << rand() << '\n';
        break;
    }
  }
  std::cout.flush();
}
