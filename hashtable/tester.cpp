#include <iostream>
#include <unordered_set>


int main() {
  char c;
  int x;
  std::unordered_set<int> s{};
  std::cout << std::boolalpha;
  while(std::cin >> c >> x) {
    switch(c){
      case 'i':
        std::cout << !s.contains(x) << std::endl;
        s.insert(x);
        break;
      case 'c':
        std::cout << s.contains(x) << std::endl;
        break;
      case 'r':
        std::cout << s.contains(x) << std::endl;
        s.erase(x);
        break;
      default:
        std::cout << "Unknown command\n";
    }
  }
}
