#include <iostream>

template<typename T, typename HASH = std::hash<T>>
class CHashTable{
public:
    CHashTable() : m_order(1), m_cap(4), m_size(0), m_arr(new CChain[1]) {}
    ~CHashTable() {
        delete [] m_arr;
    }

    bool insert(T val){
        rehash();
        return m_arr[hash(val)].insert(std::move(val));
    }

    bool remove(const T & key){
        return m_arr[hash(key)].remove(key);
    }

    bool contains(const T & key) const {
        return m_arr[hash(key)].contains(key);
    }

private:
    struct CNode {
        CNode(T data, CNode * next = nullptr) :
            m_data(data), m_next(next) {}

        T m_data;
        CNode * m_next = nullptr;
    };

    struct CChain{
        

        CChain() : head(nullptr) {}
        ~CChain() {
            CNode * n;
            while((n = pop()))
                delete n;
        }

        bool insert(T val) {
            if(contains(val))
                return false;
            push(new CNode(std::move(val), head));
            return true;
        }

        void push(CNode * n){
            head = n;
        }

        CNode * pop() {
            CNode * p = head;
            if(p)
                head = p->m_next;
            return p;
        }

        bool remove(const T & key) {
            CNode ** p = find(key);
            if(!*p)
                return false;
            CNode * n = *p;
            *p = n->m_next;
            delete n;
            return true; 
        }

        bool contains(const T & key) const {
            return *find(key);
        }

    private:
        CNode * head = nullptr;

        CNode ** find(const T & key){
            CNode ** p = &head;
            while(*p && (*p)->m_data != key)
                p = &(*p)->m_next;
            return p;
        }

        const CNode * const * find(const T & key) const{
            const CNode *const* p = &head;
            while(*p && (*p)->m_data != key)
                p = &(*p)->m_next;
            return p;
        }
    };

    inline size_t mod() const {
        return (1 << m_order) - 1;
    }

    inline size_t hash(const T & key) const {
        return HASH{}(key) % mod();
    }

    void rehash() {
        if(m_size < m_cap)
            return;

        size_t old_mod = mod();
        ++m_order;
        m_cap = mod() * 4;
        CChain * old_arr = m_arr;
        CChain * m_arr = new CChain[mod()];

        for(size_t i = 0; i < old_mod; ++i){
            CNode * n = nullptr;
            while((n = old_arr[i].pop())){
                const T & key = n->m_data;
                m_arr[hash(key)].push(n);
            }
        }
        delete [] old_arr;
    }

    //static constexpr unsigned short s_max_order = sizeof(size_t) - 1;
    unsigned short m_order = 1;
    size_t m_cap = 1, m_size = 0;
    CChain * m_arr = nullptr;

};

int main() {
    char c;
    int i;
    CHashTable<int> table{};

    std::cout << std::boolalpha;

    while(std::cin >> c >> i){
        switch (c) {
            case 'i':
                std::cout << table.insert(i) << std::endl;
                break;
            case 'c':
                std::cout << table.contains(i) << std::endl;
                break;
            case 'r':
                std::cout << table.remove(i) << std::endl;
                break;
            default:
                std::cout << "Unknown command";
        }
    }
}