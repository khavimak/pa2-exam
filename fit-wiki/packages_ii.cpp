#include<iostream>
#include<cassert>
#include<vector>
#include<set>
#include<map>
#include<string>
#include<unordered_map>
#include<memory>
#include<unordered_set>
#include<queue>
#include<list>
#include<sstream>
#include<cstring>
 
using namespace std;
 
class CPkg{
public:
    CPkg(const std::string & aname) :
        name(aname), dependecies() {}

    CPkg & addDep(const std::string & dep) {
        dependecies.push_back(dep);
        return *this;
    }

    struct Hash{
        size_t operator()(const CPkg & pkg) const{
            return std::hash<std::string>{}(pkg.name);
        }

        size_t operator()(const std::string & str) const {
            return std::hash<std::string>{}(str);
        }
    };

    friend bool operator==(const CPkg & lhs, const CPkg & rhs){
        return lhs.name == rhs.name;
    }

    const std::vector<std::string> & dep() const {
        return dependecies;
    }
 
private:
    std::string name;
    std::vector<std::string> dependecies{};
};

class CPkgSys{
public:
    CPkgSys & addPkg(const CPkg & pkg) {
        adj.insert(pkg);
        return *this;
    }
    
    set<string> install(const list<string> & l){
        std::set<string> visited{};
        std::deque<string> tovisit{};

        for(const std::string & pkg : l){
            if(!installed.contains(pkg)){
                visited.insert(pkg);
                tovisit.push_back(pkg);
            }
        }

        while(!tovisit.empty()){
            std::string curr = tovisit.front();
            tovisit.pop_front();

            
            if(auto it = adj.find(curr); it != adj.end()){
                for(const std::string & a : it->dep()){
                    if(!visited.contains(a) && !installed.contains(a)){
                        visited.insert(a);
                        tovisit.push_back(a);
                    }
                }
            }
            else
                throw std::invalid_argument("Package not found.");
        }

        for(const std::string & pkg : visited)
            installed.insert(pkg);


        return visited;
    }
 
    friend std::ostream & operator<< (std::ostream & os, const CPkgSys & sys){
        std::string del = "";

        for(const std::string & pkg : sys.installed){
            os << del << pkg;
            if(del.empty())
                del = ", ";
        }
        return os;
    }
private:

    std::unordered_set<CPkg, CPkg::Hash> adj{};
    std::set<std::string> installed{};
};
 
int main(void){
    CPkgSys s;
    stringstream ss;
    s.addPkg(CPkg("ssh").addDep("sudo").addDep("apt"))
    .addPkg(CPkg("sudo").addDep("git").addDep("c++"));
    s.addPkg(CPkg("apt"))
    .addPkg(CPkg("c++").addDep("c").addDep("asm").addDep("fortran"));
    s.addPkg(CPkg("git"))
    .addPkg(CPkg("c").addDep("kekw"))
    .addPkg(CPkg("kekw"))
    .addPkg(CPkg("asm"))
    .addPkg(CPkg("fortran"));
    s.addPkg(CPkg("python").addDep("bash").addDep("sadge"))
    .addPkg(CPkg("karel").addDep("python"))
    .addPkg(CPkg("bash").addDep("sadge"))
    .addPkg(CPkg("sadge"))
    .addPkg(CPkg("cython").addDep("dev"));
    s.addPkg(CPkg("perl"));
 
    ss << s;
    assert(ss.str() == "");
    ss.clear();
    ss.str("");
 
    set<string> t1 = s.install(list<string> {"sudo"});
    assert(t1 == (set<string> {"asm", "c", "c++", "fortran", "git", "kekw", "sudo"}));
    set<string> t2 = s.install(list<string> {"ssh", "c++"});
    assert(t2 == (set<string> {"apt", "ssh"}));
 
    ss << s;
    assert(ss.str() == "apt, asm, c, c++, fortran, git, kekw, ssh, sudo");
    ss.clear();
    ss.str("");
 
    try{
        set<string> e = s.install(list<string> {"karel", "cython"});
        assert("Sem ses nemel dostat debilku" == nullptr);
    }
    catch(const invalid_argument & e){
        assert(strcmp("Package not found.", e.what()) == 0);
    }
    set<string> t3 = s.install(list<string> {"karel", "fortran", "git"});
    assert(t3 == (set<string> {"bash", "karel", "python", "sadge"}));
 
    s.addPkg(CPkg("java").addDep("utils"))
    .addPkg(CPkg("utils").addDep("VB"))
    .addPkg(CPkg("VB").addDep("java"));
 
    set<string> t4 = s.install(list<string> {"java", "perl"});
    assert(t4 == (set<string> {"VB", "java", "perl", "utils"}));
 
    ss << s;
    assert(ss.str() == "VB, apt, asm, bash, c, c++, fortran, git, java, karel, kekw, perl, python, sadge, ssh, sudo, utils");
    ss.clear();
    ss.str("");
 
    CPkgSys k;
    k.addPkg(CPkg("ssh").addDep("sudo").addDep("apt"))
    .addPkg(CPkg("sudo").addDep("git"));
    k.addPkg(CPkg("apt"));
    k.addPkg(CPkg("git"))
    .addPkg(CPkg("c").addDep("kekw"))
    .addPkg(CPkg("kekw"))
    .addPkg(CPkg("asm"))
    .addPkg(CPkg("fortran"));
    k.addPkg(CPkg("python").addDep("bash").addDep("sadge"))
    .addPkg(CPkg("karel").addDep("python"))
    .addPkg(CPkg("bash").addDep("sadge"))
    .addPkg(CPkg("sadge"));
    k.addPkg(CPkg("perl").addDep("no"));
 
    set<string> t5 = k.install(list<string> {"asm"});
    assert(t5 == (set<string> {"asm"}));
    set<string> t6 = k.install(list<string> {"python", "ssh"});
    assert(t6 == (set<string> {"apt", "bash", "git", "python", "sadge", "ssh", "sudo"}));
 
    try{
        set<string> t7 = k.install(list<string> {"perl", "c"});
        assert("Sem ses nemel dostat debilku" == nullptr);
    }
    catch(const invalid_argument & e){
        assert(strcmp("Package not found.", e.what()) == 0);
    }
    set<string> t8 = k.install(list<string> {"c", "ssh", "karel"});
 
    assert(t8 == (set<string> {"c", "karel", "kekw"}));
 
    ss << k;
    assert(ss.str() == "apt, asm, bash, c, git, karel, kekw, python, sadge, ssh, sudo");
    ss.clear();
    ss.str("");
 
}

