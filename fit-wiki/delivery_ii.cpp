#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <memory>
#include <cassert>
using namespace std;
 
class CDelivery
{
public:
	// ctor
	// dtor
	CDelivery & addConn ( const string & a, const string & b );
	vector<int> findCoverage ( const set<string> & depots ) const;
 
private:
	std::map<std::string, std::vector<std::string>> adj{};
};
 
CDelivery & CDelivery::addConn ( const string & a, const string & b )
{
	if(auto it = adj.find(a); it != adj.end()){
        it->second.push_back(b);
    }
    else{
        adj.emplace(a, std::vector<std::string>{b});
    }

	if(auto it = adj.find(b); it != adj.end()){
        it->second.push_back(a);
    }
    else{
        adj.emplace(b, std::vector<std::string>{a});
    }

    return *this;
}
 
vector<int> CDelivery::findCoverage ( const set<string> & depots ) const
{
    for(const std::string & s : depots){
        if(!adj.contains(s))
            throw std::invalid_argument("");
    }
	vector<int> res{};

    set<string> visited{depots};
    deque<string> next{depots.begin(), depots.end()};

    while(!next.empty()){
        deque<string> tovisit = std::move(next);
        next.clear();
        res.push_back(visited.size());

        while(!tovisit.empty()){
            std::string curr = tovisit.front();
            tovisit.pop_front();

            for(const std::string & a : adj.at(curr)){
                if(!visited.contains(a)){
                    visited.insert(a);
                    next.push_back(a);
                }
            }
        }
    }

    return res;
}
 
int main ()
{
	CDelivery t0;
	vector<int> r;
	t0
	.addConn("Austin", "Berlin")
	.addConn("Chicago", "Berlin")
	.addConn("Chicago", "Dallas")
	.addConn("Dallas", "Essen")
	.addConn("Essen", "Austin")
	.addConn("Frankfurt", "Essen")
	.addConn("Gyor", "Frankfurt")
	.addConn("Helsinki", "Istanbul")
	.addConn("Istanbul", "Jakarta");
 
	r = t0.findCoverage(set<string>{"Berlin"});
	assert ( r == (vector<int>{1, 3, 5, 6, 7}) );
	/* result:
	 * [0]: Berlin = 1
	 * [1]: Austin, Berlin, Chicago = 3
	 * [2]: Austin, Berlin, Chicago, Dallas, Essen = 5
	 * [3]: Austin, Berlin, Chicago, Dallas, Essen, Frankfurt = 6
	 * [4]: Austin, Berlin, Chicago, Dallas, Essen, Frankfurt, Gyor = 7
	 */
 
	r = t0.findCoverage(set<string>{"Berlin", "Essen"});
	assert ( r == (vector<int>{2, 6, 7}) );
 
	r = t0.findCoverage(set<string>{"Helsinki"});
	assert ( r == (vector<int>{1, 2, 3}) );
 
	r = t0.findCoverage(set<string>{"Istanbul"});
	assert ( r == (vector<int>{1, 3}) );
 
	r = t0.findCoverage(set<string>{"Austin", "Jakarta"});
	assert ( r == (vector<int>{2, 5, 9, 10}) );
 
	r = t0.findCoverage(set<string>{"Chicago", "Gyor", "Helsinki", "Jakarta"});
	assert ( r == (vector<int>{4, 8, 10}) );
 
	try
	{
		r = t0.findCoverage(set<string>{"Incorrect city"});
		assert ( "No invalid_argument exception caught!" == nullptr );
	}
	catch ( const invalid_argument & e ) {}
 
	return EXIT_SUCCESS;
}

