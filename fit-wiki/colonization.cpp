#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <memory>
using namespace std;
 
class Cbase {
public:
    Cbase(string sector, string species, int when) : m_Sector(std::move(sector)), m_Species(std::move(species)), m_When(when) {}
    ~Cbase() = default;
 
    string m_Sector;
    string m_Species;
    int m_When;
};
 
#endif /* __PROGTEST__ */
 
class CUniverse {
public:
    
    CUniverse() = default;
 
    CUniverse & addConnection(const string & from, const string & to) {
        if(auto it = adj.find(from); it != adj.end())
            it->second.push_back(to);
        else
            adj.emplace(from, std::vector<std::string>{to});


        if(auto it = adj.find(to); it != adj.end())
            it->second.push_back(from);
        else
            adj.emplace(to, std::vector<std::string>{from});

        return *this;
    }
 
    CUniverse & optimize() {
        return *this;
    }
 
    map<string, string> colonise(const vector<Cbase> & bases) const {
        std::map<string, string> res{};
        std::unordered_set<string> base_set{};

        for(const Cbase & base : bases){
            res.emplace(base.m_Sector, base.m_Species);
            base_set.insert(base.m_Sector);
        }

        for(const auto & [s, l] : adj){
            if(!res.contains(s)){
                std::string spec = BFS(bases, base_set, s);
                if(spec == "") continue;
                res.emplace(s, spec);
            }
        }
        return res;
    }

private:
    
    struct NumStr{
        NumStr(size_t an = BIG_N, std::string astr = "") :
            n(an), str(astr) {}

        size_t n = BIG_N;
        std::string str{};
    };

    mutable std::unordered_map<std::string, std::vector<std::string>> adj {};

    static constexpr size_t BIG_N = 1 << 30;

    string BFS(const vector<Cbase> & bases, const unordered_set<string> & base_set, const std::string & src) const {
        std::unordered_map<string, size_t> visited{{src, 0}};
        std::deque<string> tovisit{src};

        while(!tovisit.empty()){
            string curr = tovisit.front();
            tovisit.pop_front();
            size_t time = visited.at(curr);
            for(const std::string & a : adj[curr]){
                if(!visited.contains(a)){
                    visited.emplace(a, time + 1);
                    if(!base_set.contains(a))
                        tovisit.push_back(a);
                }
            }
        }

    

        NumStr min1{}, min2{};
        for(const Cbase & base : bases){
            if(auto it = visited.find(base.m_Sector); it != visited.end()){
                upd(min1, min2, NumStr{it->second + base.m_When, base.m_Species});
            }
        }

        if(min1.n < min2.n)
            return min1.str;
        return "";
    }

    static inline void upd(NumStr & min1, NumStr & min2, NumStr && x){
        if(x.n < min1.n)
            std::swap(x, min1);
        if(x.n < min2.n)
            min2 = std::move(x);
    }
 
};
 
 
#ifndef __PROGTEST__
 
int main ( void ) {
    CUniverse u1;
 
    u1 .addConnection("Earth", "X1")
       .addConnection("Earth", "Y1")
       .addConnection("Vulcan", "X1")
       .addConnection("Vulcan", "Y2")
       .addConnection("Y2", "Y1")
       .addConnection("Kronos", "X1")
       .addConnection("X1", "X2")
       .addConnection("X2", "X3")
       .optimize();
 
    auto r1 = u1.colonise({{"Earth", "Humans", 0}, {"Vulcan", "Vulcans", 0}, {"Kronos", "Clingons", 0}});
    assert(r1 == (map<string, string> ({{"Earth", "Humans"}, {"Y1", "Humans"}, {"Vulcan", "Vulcans"}, {"Y2", "Vulcans"}, {"Kronos", "Clingons"}})));
    auto r2 = u1.colonise({{"Earth", "Humans", 0}, {"Vulcan", "Vulcans", 0}, {"Kronos", "Humans", 0}});
    assert(r2 == (map<string, string> ({{"Earth", "Humans"}, {"Y1", "Humans"}, {"Vulcan", "Vulcans"}, {"Y2", "Vulcans"}, {"Kronos", "Humans"}})));
    auto r3 = u1.colonise({{"Unknown", "Unknown", 0}});
    assert(r3 == (map<string, string> ({{"Unknown", "Unknown"}})));
    auto r4 = u1.colonise({});
    assert(r4 == (map<string, string>({})));
 
 
    CUniverse u2;
 
    u2 .addConnection("Earth", "Z1")
       .addConnection("Earth", "Y1")
       .addConnection("Earth", "Kronos")
       .addConnection("Earth", "Vulcan")
       .addConnection("Vulcan", "Y3")
       .addConnection("Vulcan", "X1")
       .addConnection("Kronos", "Z2")
       .addConnection("Kronos", "X4")
       .addConnection("Kronos", "Vulcan")
       .addConnection("Y1", "Y2")
       .addConnection("Y2", "Y3")
       .addConnection("Z1", "Z2")
       .addConnection("X1", "X2")
       .addConnection("X2", "X3")
       .addConnection("X1", "X3")
       .addConnection("X3", "X4")
       .addConnection("Bajor", "Cassiopea Prime")
       .optimize();
 
    auto r5 = u2.colonise({{"Earth", "Humans", 0}, {"Vulcan", "Vulcans", 0}, {"Kronos", "Clingons", 0}, {"Cassiopea Prime", "Cassiopeans", 0}});
    assert(r5 == (map<string, string> ({{"Earth", "Humans"}, {"Kronos", "Clingons"}, {"Vulcan", "Vulcans"}, {"Cassiopea Prime", "Cassiopeans"}, {"Bajor", "Cassiopeans"}, {"Z1", "Humans"}, {"Z2", "Clingons"}, {"Y1", "Humans"}, {"Y3", "Vulcans"}, {"X1", "Vulcans"}, {"X2", "Vulcans"}, {"X4", "Clingons"}})));
 
    //Harder tests when != 0 for all colonies
    CUniverse u3;
 
    u3 .addConnection("Earth", "Z1")
            .addConnection("Earth", "Y1")
            .addConnection("Earth", "Kronos")
            .addConnection("Earth", "Vulcan")
            .addConnection("Vulcan", "Y3")
            .addConnection("Vulcan", "X1")
            .addConnection("Kronos", "Z2")
            .addConnection("Kronos", "X4")
            .addConnection("Y1", "Y2")
            .addConnection("Y2", "Y3")
            .addConnection("Z1", "Z2")
            .addConnection("X1", "X2")
            .addConnection("X1", "X3")
            .addConnection("X2", "X3")
            .addConnection("X3", "X4")
            .addConnection("Bajor", "Cassiopea Prime")
            .optimize();
 
    auto r8 = u3.colonise({{"Earth", "Humans", 1}, {"Vulcan", "Vulcans", 0}, {"Kronos", "Clingons", 2}, {"Cassiopea Prime", "Cassiopeans", 10000}});
    assert(r8 == (map<string, string> ({{"Earth", "Humans"}, {"Kronos", "Clingons"}, {"Vulcan", "Vulcans"}, {"Y1", "Humans"}, {"Z1", "Humans"}, {"Y3", "Vulcans"}, {"Y2", "Vulcans"}, {"X1", "Vulcans"}, {"X2", "Vulcans"}, {"X3", "Vulcans"}, {"Cassiopea Prime", "Cassiopeans"}, {"Bajor", "Cassiopeans"}})));
 
    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */

