#include <string>
#include <string>
#include <iostream>
#include <sstream>
#include <cassert>
 
struct TItem
{
    TItem(std::string key, std::string val, TItem *nextHash, TItem *nextOrd)
            : m_Key(key), m_Val(val), m_NextHash(nextHash), m_NextOrder(nextOrd){}
 
    std::string m_Key, m_Val;
    TItem *m_NextHash, *m_NextOrder;
};
 
class CHash
{
public:
    CHash(int m) : m_Table(NULL), m_Size(m), m_FirstOrder(NULL), m_LastOrder(NULL)
    {
        m_Table = new TItem *[m];
        for (int i = 0; i < m; i++)
            m_Table[i] = NULL;
    }
    ~CHash()
    {
        TItem * n = m_FirstOrder, *d = n;
        while(n){
            d = n;
            n = n->m_NextOrder;
            delete d;
        }
        delete [] m_Table;
    }
    CHash(CHash &) = delete;
    CHash &operator=(CHash &) = delete;
 
    bool Insert(std::string key, std::string val)
    {
        if(IsSet(key))
            return false;

        TItem ** p = &m_Table[hashFn(key, m_Size)];
        TItem * n = new TItem(key, val, *p, nullptr);
        *p = n;

        //

        TItem * o = m_LastOrder;
        m_LastOrder = n;
        if(o)
            o->m_NextOrder = n;
        else
            m_FirstOrder = n;
        return true;
    }

    // cannot iterate through all items in table (would be too slow)
    bool IsSet(std::string key)
    {
        TItem * p = m_Table[hashFn(key, m_Size)];
        while(p && p->m_Key != key)
            p = p->m_NextHash;
        return p;
    }

    friend std::ostringstream & operator<<(std::ostringstream & os, const CHash & table) {
        TItem * n = table.m_FirstOrder;
        while(n){
            os << n->m_Key << " => " << n->m_Val <<  (n->m_NextOrder ? ", " : "");
            n = n->m_NextOrder;
        }
        return os;
    }
 
    TItem **m_Table;
    unsigned int m_Size;
    TItem *m_FirstOrder, *m_LastOrder;
private:
    unsigned int hashFn(std::string &str, size_t mod)
    {
        std::hash<std::string> hash_fn;
        return hash_fn(str) % mod;
    }
};
 
int main(int argc, char **argv)
{
    std::ostringstream oss;
    CHash hashtable(4);
    assert(hashtable.Insert("h1", "car") == true);
    assert(hashtable.Insert("h1", "phone") == false);
    assert(hashtable.Insert("h2", "field") == true);
    assert(hashtable.Insert("h3", "house") == true);
    assert(hashtable.Insert("h4", "tree") == true);
    // testing the inside of table
    // order of next_hash is up to you, tests show older items as last
    assert(hashtable.m_FirstOrder->m_Key == "h1"
           && hashtable.m_Table[0]->m_Key == "h3" && hashtable.m_Table[0]->m_Val == "house"
           && hashtable.m_Table[1]->m_Key == "h1" && hashtable.m_Table[1]->m_Val == "car"
           && hashtable.m_Table[2] == nullptr
           && hashtable.m_Table[3]->m_Key == "h4" && hashtable.m_Table[3]->m_Val == "tree"
           && hashtable.m_Table[0]->m_NextHash->m_Key == "h2" && hashtable.m_Table[0]->m_NextHash->m_Val == "field"
           && hashtable.m_Table[0]->m_NextHash->m_NextHash == nullptr
           && hashtable.m_Table[1]->m_NextHash == nullptr
           && hashtable.m_Table[3]->m_NextHash == nullptr
    );
 
    oss << hashtable;
    assert(oss.str() == "h1 => car, h2 => field, h3 => house, h4 => tree");
    return 0;
}

