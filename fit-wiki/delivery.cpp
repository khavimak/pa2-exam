#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <memory>
#include <cassert>
using namespace std;

class CDelivery
{
public:
	CDelivery() : adj{} {}

	CDelivery & addConn ( const string & from, const string & to );

	map<string, list<string>> serveCustomers ( const set<string> & customers, const set<string> & depots ) const;
 
private:
	mutable std::map<std::string, std::vector<std::string>> adj {};

    list<string> BFS(const std::string & src, const set<string> & depots) const ;
    static list<string> make_list(const std::map<std::string, std::string> & pred, const std::string & dst);
};

list<string> CDelivery::make_list(const std::map<std::string, std::string> & pred, const std::string & dst){
	std::string str = dst;
	list<string> l{dst};
	while(pred.at(str) != str){
		auto tmp = pred.at(str);
		l.push_back(tmp);
		str = tmp;
	}
	return l;
}

list<string> CDelivery::BFS(const std::string & src, const set<string> & depots) const{
	if(depots.contains(src))
		return list<string>{src};
    std::map<std::string, std::string> pred{{src, src}};
    std::deque<std::string> tovisit{src};

    while(!tovisit.empty()){
        std::string curr = tovisit.front();
        tovisit.pop_front();

        for(const std::string & a : adj[curr]){
            if(!pred.contains(a)){
                pred.emplace(a, curr);
                if(depots.contains(a))
                    return make_list(pred, a);
                tovisit.push_back(a);
            }
        }
    }

	return list<string>{};
}

CDelivery & CDelivery::addConn ( const string & to, const string & from )
{
	if(auto it = adj.find(from); it != adj.end()){
		it->second.push_back(to);
	}
	else{
		adj.emplace(from, vector<string>{to});
	}
	return *this;
}
 
map<string, list<string>> CDelivery::serveCustomers ( const set<string> & customers, const set<string> & depots ) const
{
	map<string, list<string>> res{};

	for(const std::string & customer : customers){
		res[customer] = BFS(customer, depots);
	}

	return res;
}
 
int main ()
{
	CDelivery t0;
	map<string, list<string>> r;
	t0.addConn("Austin", "Berlin");
	t0.addConn("Chicago", "Berlin");
	t0.addConn("Berlin", "Dallas");
	t0.addConn("Dallas", "Essen");
	t0.addConn("Essen", "Austin");
	t0.addConn("Frankfurt", "Gyor");
	t0.addConn("Gyor", "Helsinki");
	t0.addConn("Helsinki", "Frankfurt");
 
	r = t0.serveCustomers(set<string>{"Berlin", "Gyor"}, set<string>{"Essen", "Helsinki"});
	assert ( r == (map<string, list<string>>{ {"Berlin", {"Essen", "Austin", "Berlin"}}, {"Gyor", {"Helsinki", "Frankfurt", "Gyor"}} }) );
 
	r = t0.serveCustomers(set<string>{"Austin", "Gyor", "Chicago"}, set<string>{"Austin", "Dallas"});
	assert ( r == (map<string, list<string>>{ {"Austin", {"Austin"}}, {"Chicago", { }}, {"Gyor", { }} }) );
	t0.addConn("Chicago", "Helsinki");
 
	r = t0.serveCustomers(set<string>{"Austin", "Gyor", "Chicago"}, set<string>{"Austin", "Dallas"});
	assert ( r == (map<string, list<string>>{ {"Austin", {"Austin"}}, {"Chicago", { }}, {"Gyor", { }} }) );
	t0.addConn("Berlin", "Chicago");
 
	r = t0.serveCustomers(set<string>{"Austin", "Gyor", "Chicago"}, set<string>{"Austin", "Dallas"});
	assert ( r == (map<string, list<string>>{ {"Austin", {"Austin"}}, {"Chicago", {"Austin", "Berlin", "Chicago"}}, {"Gyor", {"Austin", "Berlin", "Chicago", "Helsinki", "Frankfurt", "Gyor"}} }) );
	t0.addConn("Essen", "Frankfurt");
 
	r = t0.serveCustomers(set<string>{"Austin", "Gyor", "Chicago"}, set<string>{"Austin", "Dallas"});
	assert ( r == (map<string, list<string>>{ {"Austin", {"Austin"}}, {"Chicago", {"Austin", "Berlin", "Chicago"}}, {"Gyor", {"Dallas", "Essen", "Frankfurt", "Gyor"}} }) );
 
	return EXIT_SUCCESS;
}

