/**
 * Implementujte metodu insert (vkládání), erase (mazání prvků
 * ze stromu a zároveň je potřeba uchovat správné pořádí podle
 * data vkládání) a vlastní destruktor, ostatní metody jsou
 * naimplentované. Kopírující konstruktor i operátor přiřazení
 * zakázán. Není povolené si přidat žádnou členskou proměnnou,
 * ale lze naimplementovat vlastní pomocné metody.
 *
 * Za správnost šablony neručím :-).
 */
#include <iostream>
#include <cassert>
#include <string>

using namespace std;

class CTree {
public:
  CTree() = default;

  ~CTree() {
    CNode *p = m_First;
    while (p) {
      CNode *d = p;
      p = p->m_NextOrder;
      delete d;
    }
  }

  CTree(const CTree &src) = delete;

  bool operator=(const CTree &other) = delete;

  bool isSet(string key) const { return *find(key); }

  bool insert(string key) {
    CNode **p = find(key);

    if (!*p) {
      *p = new CNode(key);
      update_order(*p);
      return true;
    }
    return false;
  }
  bool erase(string key) {
    //CNode::print_rec(m_Root);
    //std::cout << std::endl;
    CNode **p = find(key);

    if(!*p){
      return false;
    }


    remove_order(*p);
    CNode ** pred = predecessor(p);
    if(pred == p){
      CNode * d = *p;
      *p = d->m_R;
      delete d;
      return true;
    }
    CNode * n = *pred;
    CNode * d = *p;

    *pred = n->m_L;
    *p = n;
    n->m_L = d->m_L;
    n->m_R = d->m_R;

    delete d;
    return true;
  }

protected:
  class CNode {
  public:
    CNode(string key) : m_Key(key) {}
    string m_Key;
    CNode *m_L = nullptr;
    CNode *m_R = nullptr;
    CNode *m_PrevOrder = nullptr;
    CNode *m_NextOrder = nullptr;


    static void print_rec(CNode * n){
      if(!n)
        return;
      print_rec(n->m_L);
      std::cout << n->m_Key << ' ' ;
      print_rec(n->m_R);
    }
  };

  CNode *m_Root = nullptr;
  CNode *m_First = nullptr;
  CNode *m_Last = nullptr;

  CNode **find(const string &key) noexcept {
    CNode **n = &m_Root;
    while (*n) {
      auto cmp = (*n)->m_Key <=> key;
      if (cmp < 0)
        n = &(*n)->m_L;
      else if (cmp == 0)
        return n;
      else
        n = &(*n)->m_R;
    }
    return n;
  }

  const CNode *const *find(const string &key) const noexcept {
    const CNode *const *n = &m_Root;
    while (*n) {
      auto cmp = (*n)->m_Key <=> key;
      if (cmp < 0)
        n = &(*n)->m_L;
      else if (cmp == 0)
        return n;
      else
        n = &(*n)->m_R;
    }
    return n;
  }

  CNode **predecessor(CNode **n_par) {
    CNode **p = n_par;
    if (!(*p)->m_L)
      return p;
    p = &(*p)->m_L;
    while ((*p)->m_R)
      p = &(*p)->m_R;
    return p;
  }

  void update_order(CNode *n) {
    if (!m_Last) {
      m_First = m_Last = n;
      return;
    }

    n->m_PrevOrder = m_Last;
    m_Last = m_Last->m_NextOrder = n;
  }

  static void swap(CNode ** lhsp, CNode ** rhsp) {
    using std::swap;
    swap((*lhsp)->m_L, (*rhsp)->m_L);
    swap((*lhsp)->m_R, (*rhsp)->m_R);
  }

  void remove_order(CNode * node){
    CNode * p = node->m_PrevOrder, *n = node->m_NextOrder;
    if(m_First == node)
      m_First = n;
    if(m_Last == node)
      m_Last = p;

    if(p)
      p->m_NextOrder = n;
    if(n)
      n->m_PrevOrder = p;
  }
};

class CTester : public CTree { public:
  CTester() = default;
  void test() {
    CTester t0;
    assert(t0.insert("PA1") == true);
    assert(t0.m_First->m_Key == "PA1" && t0.m_First->m_NextOrder == nullptr);
    assert(t0.isSet("PA1") == true);
    assert(t0.insert("UOS") == true);
    assert(t0.insert("PA2") == true);
    assert(t0.isSet("PA2") == true);
    assert(t0.isSet("PA3") == false);

    assert(t0.insert("PA2") == false);
    assert(t0.insert("CAO") == true);
    assert(t0.insert("LIN") == true);
    assert(t0.insert("AAG") == true);
    assert(t0.insert("AG1") == true);
    assert(t0.insert("ZDM") == true);

    assert(
        t0.m_First->m_Key == "PA1" && t0.m_First->m_NextOrder->m_Key == "UOS" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_Key == "PA2" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "CAO" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder->m_Key ==
            "LIN" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_Key == "AAG" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_NextOrder->m_Key == "AG1" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "ZDM");

    assert(
        t0.m_Last->m_Key == "ZDM" && t0.m_Last->m_PrevOrder->m_Key == "AG1" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "AAG" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "LIN" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key ==
            "CAO" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_Key == "PA2" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_PrevOrder->m_Key == "UOS" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "PA1");

    assert(t0.erase("") == false);

    assert(t0.erase("ZDM") == true);
    assert(
        t0.m_First->m_Key == "PA1" && t0.m_First->m_NextOrder->m_Key == "UOS" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_Key == "PA2" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "CAO" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder->m_Key ==
            "LIN" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_Key == "AAG" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_NextOrder->m_Key == "AG1");
    assert(
        t0.m_Last->m_Key == "AG1" && t0.m_Last->m_PrevOrder->m_Key == "AAG" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "LIN" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "CAO" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key ==
            "PA2" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_Key == "UOS" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_PrevOrder->m_Key == "PA1");
    assert(t0.isSet("ZDM") == false);

    assert(t0.erase("AAG") == true);
    assert(
        t0.m_First->m_Key == "PA1" && t0.m_First->m_NextOrder->m_Key == "UOS" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_Key == "PA2" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "CAO" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder->m_Key ==
            "LIN" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder
                ->m_NextOrder->m_Key == "AG1");
    assert(
        t0.m_Last->m_Key == "AG1" && t0.m_Last->m_PrevOrder->m_Key == "LIN" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "CAO" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "PA2" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key ==
            "UOS" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder
                ->m_PrevOrder->m_Key == "PA1");
    assert(t0.isSet("AAG") == false);

    assert(t0.erase("CAO") == true);
    assert(
        t0.m_First->m_Key == "PA1" && t0.m_First->m_NextOrder->m_Key == "UOS" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_Key == "PA2" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "LIN" &&
        t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_NextOrder->m_Key ==
            "AG1");
    assert(
        t0.m_Last->m_Key == "AG1" && t0.m_Last->m_PrevOrder->m_Key == "LIN" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "PA2" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "UOS" &&
        t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key ==
            "PA1");
    assert(t0.isSet("CAO") == false);

    assert(t0.erase("UOS") == true);
    assert(t0.m_First->m_Key == "PA1" &&
           t0.m_First->m_NextOrder->m_Key == "PA2" &&
           t0.m_First->m_NextOrder->m_NextOrder->m_Key == "LIN" &&
           t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "AG1");
    assert(t0.m_Last->m_Key == "AG1" &&
           t0.m_Last->m_PrevOrder->m_Key == "LIN" &&
           t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "PA2" &&
           t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "PA1");
    assert(t0.isSet("UOS") == false);

    assert(t0.erase("UOS") == false);
    assert(t0.m_First->m_Key == "PA1" &&
           t0.m_First->m_NextOrder->m_Key == "PA2" &&
           t0.m_First->m_NextOrder->m_NextOrder->m_Key == "LIN" &&
           t0.m_First->m_NextOrder->m_NextOrder->m_NextOrder->m_Key == "AG1");
    assert(t0.m_Last->m_Key == "AG1" &&
           t0.m_Last->m_PrevOrder->m_Key == "LIN" &&
           t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "PA2" &&
           t0.m_Last->m_PrevOrder->m_PrevOrder->m_PrevOrder->m_Key == "PA1");
    assert(t0.isSet("UOS") == false);

    assert(t0.erase("LIN") == true);
    assert(t0.m_First->m_Key == "PA1" &&
           t0.m_First->m_NextOrder->m_Key == "PA2" &&
           t0.m_First->m_NextOrder->m_NextOrder->m_Key == "AG1");
    assert(t0.m_Last->m_Key == "AG1" &&
           t0.m_Last->m_PrevOrder->m_Key == "PA2" &&
           t0.m_Last->m_PrevOrder->m_PrevOrder->m_Key == "PA1");
    assert(t0.isSet("LIN") == false);

    assert(t0.erase("PA1") == true);
    assert(t0.m_First->m_Key == "PA2" &&
           t0.m_First->m_NextOrder->m_Key == "AG1");
    assert(t0.m_Last->m_Key == "AG1" && t0.m_Last->m_PrevOrder->m_Key == "PA2");
    assert(t0.isSet("PA1") == false);

    assert(t0.erase("PA2") == true);
    assert(t0.m_First->m_Key == "AG1");
    assert(t0.m_Last->m_Key == "AG1");
    assert(t0.isSet("PA2") == false);

    assert(t0.erase("AG1") == true);
    assert(t0.m_First == t0.m_Last);
    assert(t0.isSet("AG1") == false);
  }
};

int main() {
  CTester t;
  t.test();

  return EXIT_SUCCESS;
}
