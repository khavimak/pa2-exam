#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */
 
class CTeleport
{
public:
 
    CTeleport() : teleports(), waiting(),times() {}
 
    CTeleport & Add ( const string & from,
                      const string & to,
                      unsigned fromTime,
                      unsigned toTime )
    {
        teleports.insert({CityTime{from, fromTime}, CityTime{to, toTime}});
        times[to].push_back(toTime);
        times[from].push_back(fromTime);
        return *this;
    }
 
    CTeleport & Optimize ( void )
    {
        for(auto & [city, time] : times){
            std::ranges::sort(time);
            for(size_t i = 1; i < time.size(); ++i){
                std::cout << city << ' ' << time[i - 1] << ',' << time[i] << std::endl;
                waiting.emplace(CityTime{city, time[i - 1]}, CityTime{city, time[i]});
            }
        }

        return *this;
    }
 
    unsigned FindWay ( const string & from,
                       const string & to,
                       unsigned time )
    {
        unsigned start_time;
        try{
            auto it = std::ranges::lower_bound(times.at(from), time);
            if(it == times.at(from).end())
                throw std::out_of_range("");
            start_time = *it;
        }
        catch(const std::out_of_range &){
            if(from == to)
                return time;
            throw std::invalid_argument("");
        }
    
        std::deque<CityTime> tovisit{CityTime{from, start_time}};
        std::unordered_set<CityTime, CityTime::Hash> visited{CityTime{from, start_time}};
        unsigned min = 1 << 20;
        bool valid = false;
        while(!tovisit.empty()){
            CityTime ct = tovisit.front();
            tovisit.pop_front();

            if(ct.city == to && min > ct.time){
                valid = true;
                min = ct.time;
            }

            if(auto it = teleports.find(ct); it != teleports.end() && !visited.contains(it->second)){
                tovisit.push_back(it->second);
                visited.insert(it->second);
            }
            
            if(auto it = waiting.find(ct); it != waiting.end() && !visited.contains(it->second)){
                tovisit.push_back(it->second);
                visited.insert(it->second);
            }

        }

        if(valid)
            return min;
        throw std::invalid_argument("");
    }

private:

    struct CityTime {
        CityTime(const std::string &acity, unsigned atime)
            : city(acity), time(atime),
              hash(std::hash<std::string>{}(acity) + atime) {}


        struct Hash{
            size_t operator ()(const CityTime & ct) const {
                return ct.hash;
            }
        };

        friend bool operator==(const CityTime & lhs, const CityTime & rhs) = default;

        std::string city;
        unsigned time;
    private:
        size_t hash;
    };

    std::unordered_map<CityTime, CityTime, CityTime::Hash> teleports;
    std::unordered_map<CityTime, CityTime, CityTime::Hash> waiting;
    std::unordered_map<string, std::vector<unsigned>> times;
};
 
#ifndef __PROGTEST__
int main ( void )
{
    CTeleport t;
    t . Add ( "Prague", "Vienna", 0, 7 )
      . Add ( "Vienna", "Berlin", 9, 260 )
      . Add ( "Vienna", "London", 8, 120 )
      . Add ( "Vienna", "Chicago", 4, 3 )
      . Add ( "Prague", "Vienna", 10, 10 ) . Optimize ( );
 
    assert ( t . FindWay ( "Prague", "Vienna", 0 ) == 7 );
    assert ( t . FindWay ( "Prague", "Vienna", 1 ) == 10 );
    assert ( t . FindWay ( "Prague", "London", 0 ) == 120 );
    assert ( t . FindWay ( "Vienna", "Chicago", 4 ) == 3 );
 
    try { t . FindWay ( "Prague", "London", 2 ); assert ( "Missing exception" == nullptr ); }
    catch ( const invalid_argument & e ) { }
    catch ( ... ) { assert ( "Invalid exception" == nullptr ); }
    try { t . FindWay ( "Prague", "Chicago", 0 ); assert ( "Missing exception" == nullptr ); }
    catch ( const invalid_argument & e ) { }
    catch ( ... ) { assert ( "Invalid exception" == nullptr ); }
 
    t . Add ( "Dallas", "Atlanta", 150, 30 )
      . Add ( "Berlin", "Helsinki", 1080, 2560 )
      . Add ( "Chicago", "Frankfurt", 50, 0 )
      . Add ( "Helsinki", "Vienna", 3200, 3 )
      . Add ( "Chicago", "London", 10, 12 )
      . Add ( "London", "Atlanta", 20, 40 )
      . Add ( "Vienna", "Atlanta", 10, 50 )
      . Add ( "Prague", "Vienna", 1, 6 )
      . Add ( "Berlin", "Helsinki", 265, 265 ) 
      . Add ( "Berlin", "London", 259, 0 ) . Optimize ( );
 
    assert ( t . FindWay ( "Prague", "Frankfurt", 0 ) == 0 );
    assert ( t . FindWay ( "Prague", "Atlanta", 0 ) == 40 );
    assert ( t . FindWay ( "Prague", "Atlanta", 10 ) == 50 );
 
    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */

