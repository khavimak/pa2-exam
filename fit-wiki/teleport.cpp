/*
 * NON FUNCTIONAL
*/

#ifndef __PROGTEST__
#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#endif /* __PROGTEST__ */

class CTeleport {

public:
  CTeleport() : graph_tp(), graph_wait(), teleports() {}

  CTeleport &Add(const string &from, const string &to, unsigned fromTime,
                 unsigned toTime) {
    graph_tp.emplace(CityTime{from, fromTime}, CityTime{to, toTime});
    if(auto it = teleports.find(from); it != teleports.end()){
        it->second.push_back(fromTime);
    }
    else{
       teleports.emplace(from, std::vector<unsigned>{fromTime});
    }
    return *this;
  }

  CTeleport &Optimize(void) {
    for(auto & [city, times] : teleports){
      for(size_t i = 1; i < times.size(); ++i){
        graph_wait.emplace(std::pair{city, times[i - 1]}, std::pair{city, times[i]});
      }
    }
  }

  unsigned FindWay(const string &from, const string &to, unsigned time) {
    
    unsigned lb;

    try{
      const auto & v = teleports.at(from);
      auto it = std::lower_bound(v.begin(), v.end(), time);
      if(it == v.end())
        throw std::out_of_range("");
      lb = *it;
    }
    catch(const std::out_of_range &){
      if(from == to)
        return time;
      throw std::invalid_argument("");
    }
    
    unsigned min = 1 << 20;;
    bool valid = false;
    if(from == to){
        valid = true;
        min = time;
    }
    std::unordered_set<CityTime, CityTime::Hash> visited{{from, lb}};
    std::deque<CityTime> que{{from, lb}};

    while(!que.empty()){
        CityTime n = std::move(que.front());
        que.pop_front();

        if(n.city == to && n.time < min){
          valid = true;
          min = n.time;
        }

        if(auto it = graph_tp.find(n); it != graph_tp.end() && !visited.contains(it->second)){
          visited.insert(it->second);
          que.push_back(it->second);
        }

        if(auto it = graph_wait.find(n); it != graph_wait.end() && !visited.contains(it->second)){
          visited.insert(it->second);
          que.push_back(it->second);
        }
    }

    if(valid)
      return min;
    throw std::invalid_argument("");
  }

private:
    
  struct CityTime {
    CityTime(const std::string &acity, unsigned atime)
        : city(acity), time(atime),
          hash(std::hash<std::string>{}(acity) + atime) {}
          
    struct Hash{
        size_t operator ()(const CityTime & ct) const {
            return ct.hash;
        }
    };
    
    friend bool operator==(const CityTime & lhs, const CityTime & rhs) = default;

    std::string city;
    unsigned time;
    size_t hash;
  };

  std::unordered_map<CityTime, CityTime, CityTime::Hash> graph_tp{};
  std::unordered_map<CityTime, CityTime, CityTime::Hash> graph_wait{};
  std::unordered_map<std::string, std::vector<unsigned>> teleports {};
};

#ifndef __PROGTEST__
int main(void) {
  CTeleport t;
  t.Add("Prague", "Vienna", 0, 7)
      .Add("Vienna", "Berlin", 9, 260)
      .Add("Vienna", "London", 8, 120)
      .Add("Vienna", "Chicago", 4, 3)
      .Add("Prague", "Vienna", 10, 10)
      .Optimize();

  assert(t.FindWay("Prague", "Vienna", 0) == 7);
  assert(t.FindWay("Prague", "Vienna", 1) == 10);
  assert(t.FindWay("Prague", "London", 0) == 120);
  assert(t.FindWay("Vienna", "Chicago", 4) == 3);

  try {
    t.FindWay("Prague", "London", 2);
    assert("Missing exception" == nullptr);
  } catch (const invalid_argument &e) {
  } catch (...) {
    assert("Invalid exception" == nullptr);
  }
  try {
    t.FindWay("Prague", "Chicago", 0);
    assert("Missing exception" == nullptr);
  } catch (const invalid_argument &e) {
  } catch (...) {
    assert("Invalid exception" == nullptr);
  }

  t.Add("Dallas", "Atlanta", 150, 30)
      .Add("Berlin", "Helsinki", 1080, 2560)
      .Add("Chicago", "Frankfurt", 50, 0)
      .Add("Helsinki", "Vienna", 3200, 3)
      .Add("Chicago", "London", 10, 12)
      .Add("London", "Atlanta", 20, 40)
      .Add("Vienna", "Atlanta", 10, 50)
      .Add("Prague", "Vienna", 1, 6)
      .Add("Berlin", "Helsinki", 265, 265)
      .Add("Berlin", "London", 259, 0)
      .Optimize();

  assert(t.FindWay("Prague", "Frankfurt", 0) == 0);
  assert(t.FindWay("Prague", "Atlanta", 0) == 40);
  assert(t.FindWay("Prague", "Atlanta", 10) == 50);

  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
