#include <list>
#include <iostream>

int main() {
    std::list<int &> l{};
    int a = 1, b = 10;
    int &ar = a, &br = b;
    l.emplace_back(ar);
    l.emplace_back(br);

    a = 6666;
    for( auto i : l){
        std::cout << i << std::endl;
    }
}