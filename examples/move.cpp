#include <cstddef>
#include<iostream>

class Obj{
public:
    Obj(int d  = 0) : data(new int(d)) {}
    ~Obj() noexcept {
        delete data;
    }

    Obj(const Obj & ) = default;
    Obj(Obj && src) noexcept : data(src.data){
        src.data = nullptr; 
    }

    Obj & operator=(Obj src){
        using std::swap;
        swap(src.data, data);
        return *this;
    }

private:
    int * data = nullptr;
};

int main() {
    Obj a{10}, b{2};
    a = std::move(b);
    return 0;
}
